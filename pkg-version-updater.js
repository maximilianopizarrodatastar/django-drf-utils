const versionRegex =
  /^(?<prefix>\s*__version__ = ")(?<version>\S+)(?<suffix>".*)$/m;

module.exports.readVersion = function (contents) {
  let m = contents.match(versionRegex);
  if (!m) {
    throw new Error("No version found!");
  }
  return m.groups.version;
};

module.exports.writeVersion = function (contents, version) {
  return contents.replace(versionRegex, `$<prefix>${version}$<suffix>`);
};
