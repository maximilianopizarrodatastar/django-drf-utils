# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.7.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.6.0...2.7.0) (2022-09-21)


### Features

* **exception_logger:** log client-side exceptions at the WARNING level ([3e97c8a](https://gitlab.com/biomedit/django-drf-utils/commit/3e97c8ab0999a395073a927a281d7e2d3f7f5be2)), closes [#8](https://gitlab.com/biomedit/django-drf-utils/issues/8)

## [2.6.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.5.0...2.6.0) (2022-08-11)


### Features

* add `DetailedValidationInfo` ([74c5706](https://gitlab.com/biomedit/django-drf-utils/commit/74c57066372feb97fc1fbdf2828ac17fb13afdb9))

## [2.5.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.4.0...2.5.0) (2022-08-08)


### Features

* **config:** add support for UnionType ([08c95e0](https://gitlab.com/biomedit/django-drf-utils/commit/08c95e0e9208dba7be2b3dca1d98b5e82b15d42b)), closes [#7](https://gitlab.com/biomedit/django-drf-utils/issues/7)

## [2.4.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.3.0...2.4.0) (2022-05-23)


### Features

* do not log exceptions of class UnprocessableEntityError as ERROR ([ea7b1b8](https://gitlab.com/biomedit/django-drf-utils/commit/ea7b1b8ea4c36396ca2add3a7ed2012dacda687d)), closes [#5](https://gitlab.com/biomedit/django-drf-utils/issues/5)

## [2.3.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.2.1...2.3.0) (2022-03-28)


### Features

* **exceptions:** add `UnprocessableEntityError` ([a33109c](https://gitlab.com/biomedit/django-drf-utils/commit/a33109c23f1259290b088cadeefd667a2d4b7312))

### [2.2.1](https://gitlab.com/biomedit/django-drf-utils/compare/2.2.0...2.2.1) (2021-12-22)


### Bug Fixes

* **UniqueSchema:** improve schema generation for unique_check ([0e8b077](https://gitlab.com/biomedit/django-drf-utils/commit/0e8b0770d69e85401e4a7ed8115fc3a07acb5fd1)), closes [#3](https://gitlab.com/biomedit/django-drf-utils/issues/3)

## [2.2.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.1.0...2.2.0) (2021-12-01)


### Features

* **tests/utils:** add fixture `patch_request_side_effect` to define a side effect when mocking requests ([2f71d1d](https://gitlab.com/biomedit/django-drf-utils/commit/2f71d1d4b00dab99b48435da7f0a5f0a328bed02))


### Bug Fixes

* add [@dataclass](https://gitlab.com/dataclass) to config.BaseConfig ([704d6da](https://gitlab.com/biomedit/django-drf-utils/commit/704d6dacd9aa1a0ac628a67fbcec0bd83b75ce38))

## [2.1.0](https://gitlab.com/biomedit/django-drf-utils/compare/2.0.0...2.1.0) (2021-08-20)


### Features

* Add path field to config.Logging ([3c88fbc](https://gitlab.com/biomedit/django-drf-utils/commit/3c88fbc6391cc0ba2f4c39f9196507b419caef96))

## [2.0.0](https://gitlab.com/biomedit/django-drf-utils/compare/1.2.2...2.0.0) (2021-08-11)


### ⚠ BREAKING CHANGES

* **email.sendmail:** email.sendmail no longer uses settings.CONFIG.email

The `sendmail` function no longer depends on `settings.CONFIG`,
which might not be defined in the project. The function now accepts
the explicit `email_cfg` argument.

### Features

* add config module ([ebf0fcc](https://gitlab.com/biomedit/django-drf-utils/commit/ebf0fcc360c94c531c35f6edff12dc1cc2f46a3d))


### Bug Fixes

* **config.BaseConfig:** do not modify the input dictionary ([77dc76c](https://gitlab.com/biomedit/django-drf-utils/commit/77dc76cf9f51c1efc28e78c34e4ad6e9f1e93ded))
* **email.sendmail:** remove implicit dependency on settings.CONFIG.email ([5e994f3](https://gitlab.com/biomedit/django-drf-utils/commit/5e994f36d4e5d829389cfe36c2f727259fb15ad6))

### [1.2.2](https://gitlab.com/biomedit/django-drf-utils/compare/1.2.1...1.2.2) (2021-07-13)

### Bug Fixes

- **unique_check:** ensure no error is thrown when a field is not unique ([cece186](https://gitlab.com/biomedit/django-drf-utils/commit/cece186995f92f8d22131534e4de29f919798537)), closes [portal#395](https://gitlab.com/biwg/libweb/portal/issues/395)

### [1.2.1](https://gitlab.com/biomedit/django-drf-utils/compare/1.2.0...1.2.1) (2021-07-09)

## [1.2.0](https://gitlab.com/biomedit/django-drf-utils/compare/1.1.0...1.2.0) (2021-07-09)

### Features

- **models:** add some utility methods and classes related to models ([a0ff7ee](https://gitlab.com/biomedit/django-drf-utils/commit/a0ff7eeee9a2e2c03089002a67076aa151612cba))

## [1.1.0](https://gitlab.com/biomedit/django-drf-utils/compare/1.0.3...1.1.0) (2021-06-28)

### Features

- **serializers/utils:** add utility method `update_related_fields` ([648c3c3](https://gitlab.com/biomedit/django-drf-utils/commit/648c3c3cb8b65dc575b5fcc59dc51df21007ba44))

### [1.0.3](https://gitlab.com/biomedit/django-drf-utils/compare/1.0.2...1.0.3) (2021-06-23)

### [1.0.2](https://gitlab.com/biomedit/django-drf-utils/compare/1.0.1...1.0.2) (2021-06-04)

### Bug Fixes

- move test utils to the main package ([c0c5fee](https://gitlab.com/biomedit/django-drf-utils/commit/c0c5fee713369bd078ed4f556c71f786e33f42cc))

### [1.0.1](https://gitlab.com/biomedit/django-drf-utils/compare/1.0.0...1.0.1) (2021-06-03)

## 1.0.0 (2021-06-01)
